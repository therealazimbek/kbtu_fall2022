let productsList = [
  {
    name: "iPhone 12 Gray 256GB",
    price: 680,
    count: 10,
  },
  {
    name: "iPhone 13 White 512GB",
    price: 780,
    count: 5,
  },
  {
    name: "iPhone 14 Pro Black 512GB",
    price: 990,
    count: 2,
  },
  {
    name: "iPhone 14 Black 256GB",
    price: 890,
    count: 6,
  },
  {
    name: "Samsung S21 Black 256GB",
    price: 670,
    count: 9,
  },
  {
    name: "Samsung S22 White 256GB",
    price: 770,
    count: 4,
  },
  {
    name: "Samsung A52 Pink 256GB",
    price: 400,
    count: 15,
  },
  {
    name: "Samsung S22 Ultra Black 256GB",
    price: 1090,
    count: 7,
  },
];
let productsListAdmin = [
  {
    name: "iPhone 12 Gray 256GB",
    price: 680,
    count: 10,
    actual: 650,
  },
  {
    name: "iPhone 13 White 512GB",
    price: 780,
    count: 5,
    actual: 750,
  },
  {
    name: "iPhone 14 Pro Black 512GB",
    price: 990,
    count: 2,
    actual: 960,
  },
  {
    name: "iPhone 14 Black 256GB",
    price: 890,
    count: 6,
    actual: 850,
  },
  {
    name: "Samsung S21 Black 256GB",
    price: 670,
    count: 9,
    actual: 630,
  },
  {
    name: "Samsung S22 White 256GB",
    price: 770,
    count: 4,
    actual: 730,
  },
  {
    name: "Samsung A52 Pink 256GB",
    price: 400,
    count: 15,
    actual: 690,
  },
  {
    name: "Samsung S22 Ultra Black 256GB",
    price: 1090,
    count: 7,
    actual: 1000,
  },
];

const admin = {
  name: "admin",
  password: "admin",
};

const buyer = {
  name: "buyer",
  password: "buyer",
};

let adminRole = false;
let buyerRole = false;
let firstTime = true;

let cardList = [];

let buyerMode = document.querySelector(".buyerMode");
let adminMode = document.querySelector(".adminMode");
buyerMode.style.display = "none";
adminMode.style.display = "none";

const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-form-submit");
loginButton.addEventListener("click", (e) => {
  e.preventDefault();
  const username = loginForm.username.value;
  const password = loginForm.password.value;

  if (username === admin.name && password === admin.password) {
    buyerRole = false;
    adminRole = true;
    buyerMode.style.display = "none";
    loginForm.username.value = "";
    loginForm.password.value = "";
    adminRoleView();
  } else if (username === buyer.name && password === buyer.password) {
    adminRole = false;
    buyerRole = true;
    adminMode.style.display = "none";
    loginForm.username.value = "";
    loginForm.password.value = "";
    buyerRoleView();
  }
});

// function login() {
//   let name = prompt("Role:");
//   let password = prompt("Password");
//   if (name === admin.name && password === admin.password) {
//     adminRole = true;
//   } else if (name === buyer.name && password === buyer.password) {
//     buyerRole = true;
//   }
//
//   run();
// }

function adminRoleView() {
  adminMode.style.display = "block";

  console.log(cardList);
  console.log(productsList);
  console.log(productsListAdmin);

  let changes = [];

  for (let i = 0; i < productsListAdmin.length; i++) {
    if (productsListAdmin[i].count - productsList[i].count !== 0) {
      changes.push({
        name: productsListAdmin[i].name,
        price: productsListAdmin[i].price,
        actual: productsListAdmin[i].actual,
        count: productsListAdmin[i].count - productsList[i].count,
      });
    }
  }
  console.log(changes);
  let sold = document.querySelector(".sold");
  let totalSold = 0;
  let actualTotal = 0;

  for (let i = 0; i < changes.length; i++) {
    totalSold += parseInt(changes[i].price) * parseInt(changes[i].count);
    actualTotal += parseInt(changes[i].actual) * parseInt(changes[i].count);

    let product = document.createElement("div");
    product.className = "product";

    let h2 = document.createElement("h2");
    h2.innerText = changes[i].name;

    let c = document.createElement("p");
    c.innerText = "Sold " + changes[i].count;

    product.append(h2);
    product.append(c);
    sold.append(product);
  }

  console.log(totalSold);
  console.log(actualTotal);

  let tot = document.querySelector(".totalSold");
  tot.innerText = "Total sales: " + totalSold + "$";

  let act = document.querySelector(".actualTotal");
  act.innerText = "Actual total: " + actualTotal + "$";

  let inc = document.querySelector(".incomeTotal");
  inc.innerText = "Total sales: " + (totalSold - actualTotal) + "$";
}

function buyerRoleView() {
  buyerMode.style.display = "block";

  let products = document.querySelector(".products");
  if (firstTime === true) {
    for (let i = 0; i < productsList.length; i++) {
      let product = document.createElement("div");
      product.className = "product";

      let h2 = document.createElement("h2");
      h2.innerText = productsList[i].name;

      let price = document.createElement("h4");
      price.className = "price";
      price.innerText = productsList[i].price;

      let currency = document.createElement("span");
      currency.innerText = "$";

      let button = document.createElement("button");
      button.className = "add";
      button.innerText = "Add to Card";

      product.append(h2);
      product.append(price);
      product.append(button);
      products.append(product);
    }
    firstTime = false;
  }

  let card = document.querySelector(".card");
  card.style.display = "none";
  let isShown = false;

  let cardButton = document.querySelector(".show-card");
  cardButton.addEventListener("click", () => {
    isShown = !isShown;
    if (isShown) {
      card.style.display = "block";
      let totalSum = 0;
      for (let i = 0; i < cardList.length; i++) {
        totalSum += +cardList[i].price * +cardList[i].count;
      }
      console.log(totalSum);
      console.log(cardList);
      checkout(card, totalSum);
    } else {
      card.style.display = "none";
    }
  });

  products.addEventListener("click", (event) => {
    let element = event.target;
    if (element.tagName.toLowerCase() === "button") {
      addToCard(element);
    }
  });
}

// if (adminRole) {
//   adminRoleView();
// } else if (buyerRole) {
//   buyerRoleView();
// } else {
//   content.innerText = "Login as Admin/Buyer";
// }

let addToCard = (element) => {
  let parent = element.parentNode;
  let productName = parent.querySelector("h2").innerText;
  let productPrice = +parent.querySelector("h4").innerText;
  let exists = false;
  let outOfStock = false;

  let available = productsList.find((p) => p.name === productName).count;

  for (let i = 0; i < cardList.length; i++) {
    if (cardList[i].count + 1 > available) {
      alert("Not enough item");
      outOfStock = true;
      break;
    } else if (cardList[i].name === productName) {
      cardList[i].count += 1;
      exists = true;
      alert(
        `Added: ${productName}, Count: ${cardList[i].count}, Remaining: ${
          available - cardList[i].count
        }`
      );
    }
  }

  if (!outOfStock && !exists && available >= 1) {
    cardList.push({
      name: productName,
      price: productPrice,
      count: 1,
    });
    alert(`Added: ${productName}, Count: 1, Remaining: ${available - 1}`);
  }
};

function checkout(card, totalSum) {
  card.querySelector(".total").innerText = "Total: " + totalSum + "$";
  let buyButton = card.querySelector(".buy");

  buyButton.addEventListener("click", () => {
    if (cardList.length > 0) {
      for (let i = 0; i < cardList.length; i++) {
        productsList.find((p, j) => {
          if (p.name === cardList[i].name) {
            productsList[j].count -= cardList[i].count;
            console.log(productsList[j].count);
            return true;
          }
        });
      }
      cardList = [];
      alert("Successfully purchased!");
    } else {
      alert("No items in card!");
    }
  });
}

// login();

// function run() {
//   if (adminRole) {
//     adminRoleView();
//   } else if (buyerRole) {
//     buyerRoleView();
//   }
// }
